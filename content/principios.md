+++
date = "2016-07-01T10:30:44-03:00"
menu = "main"
title = "Princípios"
type = "about"
weight = -210
+++

## Conheça nossos princípios

### 1. Automação

Automatizar para ter controle, visão e produtividade.

- Gerência de Configurações
    - Convergência
    - Controle de estados
    - Idempotência
    - Versionamento
    - Testes
       - Syntax
       - Linter
       - Comportamento
       - Aceitação
- Provisionamento
    - Autoserviço
       - Bare Metal
       - VMs
       - Containers
- Orquestração
    - Push
    - MQ

Não existem mais sistemas simples, todos os sistemas são complexos e interdependentes. Hoje em dia manter um simples blog significa manter serviços como servidor de aplicação, banco de dados, cache, autenticação, estatísticas, replicação, dns, firewall, backup dentre outros componentes.

Administrar tudo isso manualmente
se torna inviável pois um cliente pode rodar centenas ou milhares de sites, serviços e sistemas com características similares a estas.

Dentro deste contexto, podemos dizer que a automação é um dos principais pilares da Infra Ágil pois não há mais espaço para administrar sistemas e serviços no modelo artesão.

Automação se torna obrigatória principalmente em ambientes que utilizam virtualização, nuvem ou containers.

#### 1.1 Gerência de Configurações

Atualmente quando falamos de automação em grande escala, o principal modelo que vem sendo utilizado é a gerência de configurações (GCONF). Este modelo foi criado por Mark Burgesss, professor e pesquisador
da Universidade de OSLO na Bélgica.

Em 1993 ele criou o conceito GCONF e escreveu a primeira ferramenta (CFEngine) com base nos seguintes princípios:

- Controle de estados
- Convergência

Em 2003 o mesmo professor definiu novos princípios através da teoria Promisse que organiza, expande e define os princípios computacionais básicos utilizados em CGONF, dentre eles podemos destacar:

- Controle de estados
- Convergência
- Enforcing
- Idempotência
- Self-healing
- Reporting
- Agents

A partir da publicação desta teoria, diversas ferramentas de GCONF foram criadas. Além do CFEngine que foi criado pelo professor, podemos destacar entre as principais e pioneiras o Puppet seguidas então por Chef e mais recentemente Salt.

Ferramentas de gerência de configuração tem as seguintes características:

1. Funcionam em arquitetura agente e servidor;
2. O servidor armazena as configurações dos nodes;
3. O agente que roda no node busca as configurações no servidor e aplica no node;
4. O agente funciona com o conceito de gerência de estados;
5. Você usa uma linguagem declarativa para descrever os estados desejados;
6. Idempotência é uma princípio fundamental do agente;
7. Há um robusto sistema de relatórios e registro de eventos e mudanças;
8. O servidor nunca se conecta nos nós, apenas os nós se conectam ao servidor.

#### Infraestrutrua como código

Com a gerência de configurações a organização entra no mundo da infraestrutura como código e passa a utilizar também ferramentas de controle de versão para gerenciar o código que representa sua infraestrutrua. Isto possibilita o uso de diversos métodos e ferramentas de testes para código. Todo esse controle ajuda a garantir qualidade do código que será utilizado.

#### Benefícios

Dentre os principais benefícios deste modelo estão garantia de compliance de todo o seu ambiente, self-healing, visibilidade plena das mudanças através de relatórios robustos, produtividade no processo de automação utilizando linguagens com abstração, velocidade e assertividade na configuração de sistemas e serviços, redução no índice de incidentes e dimunuição dos riscos em mudanças.

Uma sysadmin que utiliza uma ferramentas de GCONF pode facilmente administrar milhares de nós, equanto um sysadmin artesão dificilmente conseguirá manter o compliance de meros 20 nós.

O uso deste tipo de ferramental vai permitir que seu time de especialistas repasse as atividades repetitivas de
cotidianas para a ferramenta, que tem melhores condições de manter o compliance, enquanto ele se concentra em atividades mais úteis para seu time e para sua organização.

Em uma Infra Ágil o uso de GCONF é essencial.

#### 1.2 Provisionamento

Na maioria dos projetos em que atuamos o provisionamento é um ponto muito sensível da organização.

Encontramos organizações que levavam até 90 dias para entregar uma simples VM para um time de desenvolvimento
fazer testes. Isto ocorre devido a processos lentos, burocráticos, ineficientes e pelo uso de método artesão para criação e configuração destes ambientes ao final do processo.

Em Infra Ágil a criação de novos ambientes deve ser um processo simples e deve levar minutos ao invés de horas, dias, semanas ou meses. Para isto são combinados alguns fatores:

- Autoserviço
- Cotas
- Automação

A utilização ferramentas de autoserviço permite que o cliente possa criar seu ambiente diretamente. Quando falamos de um time de TI, criar VMs é algo natural, profissionais de TI estão familiarizados com a criação de VMs ou instâncias na nuvem para testes ou estudos, portanto, os processos de criação de VM no hypervisor da empresa, criação instâncias na nuvem interna ou de containers, devem ser processos naturais e simples. Não faz sentido burocratizar uma demanda cotidiana e inserir pessoas entre a necessidade e o recurso de TI a ser utilizado.

Existem diversas ferramentas de autoserviço que podem ser utilizadas e cada time pode
ter suas cotas de uso do hypervisor/nuvem/container/storage o que garante o uso simplificado e ao mesmo tempo protege, controla e condiciona o uso racional dos recursos de TI da organização.

Para chegar neste resultado, as ferramentas de automação são integradas ao sistema de autoserviço para criar VMs/Instâncias/Containers de forma automatizada, esta ação converge todos os requisitos em um único processo que fará o bootstrap, instalação do sistema operacional, configuração de rede, regras de firewall, disco, memoria, processador dentre outras características.

Em Infra Ágil o provisionamento de ambientes deve estar acessível a todos do time de TI de forma inteligente, rápida e segura.

#### 1.3 Orquestração

Orquestração é um termo com diferentes concepções e entendimentos no mundo de TI, apesar da origem do termo
vir de uma orquestra, onde cada músico tem consigo o "código" que precisa executar, na TI a orientação
dada ao termo tem outro ponto de vista, hoje orquestração tem como base principal um local central que dispara
ações em paralelo para sistemas operacionais. Atualmente diversos fabricantes e fornecedores corporativos seguem essa linha.

Para nós, dentro de nosso modelo, vamos considerar que orquestração consiste em executar algo de forma paralela ou não, em tempo real, em um sistema operacional ou em um grupo de sistemas operacionais.

Esse tipo de tecnologia também é muito utilizada para administração de sistemas e agrega valor complementando
e apoiando as ferramentas de GCONF.

#### Orquestração em arquitetura PUSH

As ferramentas de orquestração em arquitetura PUSH normalmente utilizam o protocolo SSH para se conectar aos nós e executar ações, dentre elas podemos citar Ansible, Capistrano e Fabric. Elas dependem integralmente de um protocolo externo, não usam agentes locais e no seu desenho a gerência de estados e idempotência não é uma premissa nativa.

#### Orquestração em Arquitetura MQ

As ferramentas de orquestração em arquitetura mensageria utilizam ferramentas MQ, neste modelo a ferramenta
consome uma fila ou tópico que pode ter comandos injetados pelo sysadmin. Uma vez que comandos são injetados
na fila, o agente orquestrador recebe, valida e executa em seu node. O Marionette Mcollective é um exemplo de ferramenta deste tipo, ele usa o ActiveMQ como MQ, outro exemplo conhecido é o Salt, ele tem um subsistema de orquestração que usa o projeto ZeroMQ para orquestrar seus minions.

#### Benefícios

Ferramentas de orquestração podem ser utilizadas para fazer deploy de aplicações, manutenções pontuais e emergenciais em seus nodes e também é utilizada para fazer manutenção e rollout de agentes GCONF como Puppe
e Chef.

São ferramentas importantes para se ter em seu toolkit de sysadmin.

### 2. Entrega

Foco em uma entrega com qualidade, automatizada e rápida.

- Controle de versão
- Repositório de artefatos
- CD/CI/QA
- Testes
    - TDD
    - BDD
    - ATDD
    - Funcionais/WEB
    - Load/Stress

#### Nivelamento

É importante entender o que não é entrega:

- Receber arquivos anexos via e-mail para publicar em N nodes;
- Editar arquivos no servidor diretamente;
- Publicar algo que não passou por testes ou QA;
- Publicar algo que não tenha plano de rollback.

Os exemplos acima demonstram a triste realidade de processos de entrega artesenais
em TI clássica.

Não há necessidade de expor seu cliente a riscos desnecessários.

Em Infra Ágil devemos fazer a entrega de novas versões de uma aplicação de forma
segura, eficiente e com baixo risco.

#### Fazendo do jeito correto

Para algo ser publicado, antes deve ocorrer um rigoroso processo de testes, após estes testes - caso a aplicação seja aprovada em todos, o artefato deve ser armazenado em local adequado para permitir a automação da etapa final de publicação. Se necessário será realizado rollback para uma versão anterior também disponível neste mesmo repositório.

Exitem diversas técnicas que podem ser utilizadas para criar uma esteira de entrega, dentre elas podemos citar Continuous Delivery, Continous Deployment e Continous Integration.

Em relação aos testes todas as aplicações devem passar por diversos testes dentro desta esteira. Devem ser invocados desde testes unitários, testes de aceitação, testes de carga e estresse e até testes funcionais pós-publicação.

UNITARIO > ACEITACAO > CARGA > FUNCIONAL

Publicar sem testar é trabalho amador e expõe os usuários de seu cliente a um comportamente imprevisível.

#### Versionamento

Todo o código deve estar sempre em sistemas de controle de versão, não há exceções. Todos os times de TI devem usar o mesmo sistema e todos devem ter acesso amplo nos repositórios internos.

#### Deveres e poderes

Quem deve dizer se uma aplicação está pronta ou não para ser publicada são os testes e não o time de Sysadmins.

Em Infra Ágil não há chamados de Deploy para o time de Infra, entenda que o controle da entrega não passa pelos sysadmins, cabe a eles apenas manter a esteira de entrega funcionando, o resto é com a área de negócios.

Os Sysadmins devem automatizar essa esteira com base em requisitos que devem vir da área de negócio.

#### Integração entre times

A criação da esteira normalmente é um processo coletivo entre Infra e Devel, algo muito comum em iniciativas DevOps, mas em determinados casos, em ambientes mais simples, a própria área de infra pode criar uma esteira básica de publicação para facilitar seu trabalho.

### 3. Métricas

Não há gerência ou administração de infraestrutura sem métricas.

 - Gerar dados
 - Coletar dados
 - Processar dados
 - Armazenar dados
 - Visualizar dados
 - Transformar dado em informação
 - Consumir informações

 Sysadmins normalmente focam muito na monitoração da infraestrutra básica e se esquecem das métricas mais
 importantes para o negócio de seus clientes.

 Monitorar é importante, eu diria até essencial, contudo, nem sempre conseguimos monitorar o que precisamos pois certos dados não existem e precisam ser gerados para que possamos entendê-los, estudá-los e tomar ações a partir deles.

 Para ficar claro, seguem algumas perguntas:

 - Como você sabe se a performance da principal app do seu cliente melhorou ou piorou após a última publicação?
 - Como você mensura a quantidade real de clientes conectados a sua aplicação?
 - Como você mensura a saúde geral de sua aplicação?

Veja que isso vai muito além de checar processo, porta em servidor ou string de retorno de alguma página.

Determinadas métricas são vão existir se nós as criarmos.

Determinadas informações só poderão ser consumidas se houver métricas para sustentá-las.

Métricas precisam ser geradas, coletadas e armazenas em local adequado.

Existindo tais métricas podemos criar informações realmente úteis sobre nossas aplicações e infraestrutura.

É muito importante que a aplicação desde a sua concepção já pense em métricas e informações para que
seja possível medir sua saúde e informações estruturais.

#### Integrações

Integre seu monitoramento com sua automação e provisionamento, suas métricas podem
ser o gatilho para processos internos de escalabilidade ou redução de recursos no
caso de pouca demanda.

#### Controle

Em infra ágil as métricas são os fundamentos da gestão de sua infraestrutura, elas
vão possibilitar planejamento, vão te te permitir análisar cenários e incidentes e vão
proporcionar respostas importantes para seus clientes e usuários.

### 4. Pessoas

 Pessoas tem que se entender parte do time e se integrar a ele.

 - Aplicação de métodos ágeis
 - Integração do time
 - Nivelamento e compartilhamento
 - Definição de valores
 - Definição de objetivos

Depois de falarmos de tantos modelos e tecnologias precisamos falar de pessoas, afinal não é possível ter tantos
modelos, tecnologias e continuar funcionando em uma estrutura de equipe clássica.

Talvez a parte mais sensível de uma iniciativa de infra ágil seja a mudança na forma da equipe se comunicar e trabalhar.

Neste modelo as pessoas precisam se comunicar, se expressar, precisam ter liberdade criativa, precisam de direcionamento e principalmente de foco.

Em uma equipe ágil precisamos:

1. Incentivar o compartilhamento de conhecimento

2. Incentivar a integração de sua equipe

3. Ajudar sua equipe a encontrar o que ela precisa para se motivar

4. Oferecer os meios para que eles tenham direcionamento e consciência do trabalho

4. Incentivar a criatividade e a inovação

Para ajudar seu time, faça um planejamento colaborativo de curto, médio e longo prazo

Tentem responder as seguintes perguntas:

- Qual o estado desejado de nossa infra daqui a 3, 6, 9 e 12, 24 meses?
- O que nossa organização está buscando em 24 meses?
- Qual resultado estamos buscando como equipe?

Ajude-os a enxergar o caminho a ser percorrido.

Se as pessoas estão motivadas, se elas tem liberdade criativa, sem tem incentivo e direcionamento, isso se reflete em resultados positivos para o profissional, para a equipe e para a organização.

Métodos que podem ser utilizados:

- Kanban
- Scrum
- Dojos
- Games
- Hacklabs
- Hackatons
- Quaisquer métodos ágeis

Outros:

- Prática de esportes coletivos
- Práticas coletivas recreativas

Adapte os métodos a sua organização e ao seu time.

A integração do seu time deve acontecer dentro e fora do local de trabalho.

Apesar de todos os métodos e tecnologias, o importante são as sempre as pessoas, são elas que fazem seu negócio fluir, sua organização não existe sem as pessoas, lembre-se disto!

## Sobre os princípios

 Essa construção de princípios já foi melhorada e modernizada algumas vezes, ela já contou com a contribuição de Miguel Filho, José Júnior e Fernando Ike. Espero que outros especialistas nos ajudem a avançar ainda mais.
