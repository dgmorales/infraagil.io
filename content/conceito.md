+++
date = "2016-06-30T22:36:15-03:00"
menu = "main"
title = "Conceito"
type = "about"
weight = -240
+++

## Entenda o conceito
##### por: Guto Carvalho

Infra Ágil é essencialmente um guia de adoção de boas práticas a ser utilizada dentro do seu time de operação, partindo da visão de seniors sysadmins conectados aos modelos ágeis.

A ideia principal é que este modelo auxilie times de operação a executar uma transição segura dentro de sua infra para um cenário mais eficiente  e autônomo.

Infra Ágil tem como fundamentos principais infraestrutura como código, automação, entrega, métricas e métodos ágeis para a integração de seus times.

O modelo está organizado de forma coerente e simples para facilitar sua adoção.
