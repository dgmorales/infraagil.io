+++
date = "2016-06-30T22:59:32-03:00"
menu = "main"
title = "DevOps?"
type = "about"
weight = -220
+++

## Infra Ágil ou DevOps?
##### por: guto carvalho

Qual a diferença entre os dois?

Especialmente no Brasil a confusão sobre DevOps é terrível, tem gente que acha que DevOps é um programador que sabe infra, ou que é um sysadmin que sabe programar, mas no fim não é nada isso. DevOps não é um cargo, não é uma pessoa, DevOps é essencialmente uma CULTURA que busca integração entre os times de TI de uma organização.

DevOps foi criado com o foco em entregar um software melhor, mais rápido, com menor risco, de forma automatizada e controlada. Este movimento é fundamentado nisto, mas antes disso tudo, a CULTURA estabelece uma série de modelos para resolver os problemas internos da organização que a IMPEDEM de entregar softwares melhores, mais rápidos, com menor risco e de forma automatizada.

Antes da tecnologia vem o problema a ser assumido, resolvido, uma cultura a ser mudada para que todas as tecnologias e métodos possam ajudar a organização.

DevOps é algo bastante amplo que depende muito da maturidade da organização para que este tipo
de iniciativa avance.

### Adoção da cultura DevOps  no Brasil

Infelizmente nem sempre a organização está disposta a assumir seus erros ou a mudar a sua cultura, principalmente se for necessário reorganizar toda a sua estrutura de TI para adotar as práticas DevOps.

Eu trabalhei em muitos ministérios, empresas públicas federais e multi-nacionais com estruturas altamente hierarquizadas, verticais, com problemas políticos e culturais graves que impossibilitavam a adoção desse tipo de modelo.

Em algumas destas empresas, mesmo após muitas tentativas, eu não consegui sensibilizar times, pessoas e gestores, em especial em locais com diretorias separadas para desenvolvimento e infraestrutura, locais em que times estavam com a relação tão desgastada que não havia espaço e interesse para conversarem entre si acerca do compartilhamento responsabilidades, problemas e soluções.

Em alguns lugares a simples menção do termo DevOps, Cultura DevOps ou práticas DevOps já causava pânico e desconforto entre fornecedores, gestores, diretores e prestadores de serviço.

Algumas destas organizações possuíam diferentes contratos para áreas de infraestrutura e desenvolvimento, já vi empresas com 5 contratos de fábricas de softwares e 3 contratos de infraestrutura divididos entre operação, monitoração e suporte, então imagine um consultor tentar iniciar uma discussão acerca de integração de esforços e divisão de responsabilidades com 8 empresas diferentes, tarefa das mais árduas.

Nestes cenários não há milagre, contudo, eu ainda tinha vontade de pelo menos tentar transformar a realidade do time local de infra, deveria haver uma forma de aplicar um novo modelo pelo menos em times isolados.

Após muitas tentativas, encontrei corajosos gestores que mesmo nessas organizações problemáticas, ofereceram espaço para que eu pudesse introduzir métodos ágeis nos times que cuidavam do core de infraestrutura, achei uma via e aproveitei.

### Nova abordagem 

Após autorização destes gestores, comecei a implantar métodos ágeis e soluções de automação dentro de times de infraestrutura, foram várias experiências entre 2012 e 2015, todas elas me ajudaram a consolidar esse aprendizado e compartilhar o que havia vivido em diversas palestras apresentadas em conferências de TI pelo Brasil.

Evitei usar o nome DevOps nestas palestras pois a Buzzword estava em um momento confuso no país naqueles anos, cada um puxava para um lado, ou era CI/CD, ou era simplesmente automação de infraestrutura.

Por ser um entusiasta do movimento DevOps, leitor assíduo de artigos dos fundadores do movimento, e por saber de suas origens, nunca achei justo qualificar DevOps como um único método ou algum tipo de tecnologia.

Para mim, DevOps é uma cultura que leva a um processo de transformação de **toda a organização**, e se o processo for conduzido com apoio da direção, e se contar com a adesão dos times de TI, isto trará certamente resultados fantásticos para a organização.

### Nova identidade 

Dito isto, usar o termo DevOps para identificar a transformação local de um time de operação não me parece correto, portanto, usar o termo Infra Ágil neste contexto passa
de forma clara a ideia da tranformação que se deseja alcançar, além de ser mais palatável para os gestores e  profissionais envolvidos, evitando assim a criação de barreiras políticas e ideológicas durante o projeto.

Mesmo sendo movimentos distintos, ainda assim, o modelo **Infra Ágil** se inspira profundamente na cultura DevOps, um não conflita com o outro, são complementares e importantes para qualquer processo de renovação e melhoria nos times e operação.
