+++
date = "2016-07-04T09:48:38-03:00"
menu = "main"
title = "podcast"
type = "about"
weight = -150
+++

Nosso podcast semanal de Infra Ágil.

### Formato

- Podcast 00X - Infra Ágil: Eixo Pessoas
    - Assunto 1
    - Assunto 2
- Podcast 00X - Infra Ágil: Eixo Automação
    - Assunto 1
    - Assunto 2
- Podcast 00X - Infra Ágil: Eixo Entrega
    - Assunto 1
- Podcast 00X - Infra Ágil: Eixo Métricas
    - Assunto 1

## Agendas

#### Mark Zero

- Podcast Zero - Infra Ágil: Conceito e Prática (data a definir)

#### Mark 1

- Podcast 001 - Eixo Automação (data a definir)
  - Mark Burgess e a teoria promisse
     - Entenda Idempotência
     - Entenda Gerência de estados
  - Provisionamento, como deveria ser?
  - Orquestração, o que é?
  - Sugestão de soluções
- Podcast 002 - Eixo Métricas (data a definir)
     - Você enxerga sua infra?
     - Monitoração, como deveria ser?
     - Métricas, como deveria ser?
     - Sugestão de soluções
- Podcast 003 - Eixo Entrega (data a definir)
  - Deploy anti-pattern
  - Entenda CD e CI
  - Tipos de testes
  - Sugestão de soluções
- Podcast 004 - Eixo Pessoas (data a definir)
  - Métodos ágeis para Ops
  - Muito além dos métodos
  - Pessoas são importantes
  - Sugestões gerais

#### Mark 2

- Gerência de nuvem, como fazer?
- Gerência de credenciais de apps, como fazer?
- Containers, entenda e use do jeito certo!
- Cluster de containers, fatos e alternativas
- Apps essenciais para seu time de infra ágil

Mande suas sugestões!
