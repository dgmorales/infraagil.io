+++
categories = ["core"]
date = "2016-06-30T21:54:54-03:00"
description = ""
draft = false
image = "/img/about-bg.jpg"
title = ""
tags = ["conceito"]
+++

##### Por: Guto Carvalho

Hoje em dia muita gente fala sobre "infraestrutura ágil" no Brasil e normalmente estas pessoas fazem um link com a cultura DevOps e suas vertentes.

Alguns profissionais, ao falar de infraestrutura ágil acabam focando na parte de automação, outros na parte de métricas, outros na parte de deploy, há muitas abordagens e diferentes metodologias sendo utilizadas, toda essa informação dificulta o entendimento e a adoção por parte de novos entusiastas.

Ainda não há um consenso sobre o assunto, portanto, atendendo a pedidos criei este site para facilitar o caminho de novos entusiastas que desejam melhorar e renovar as práticas do time operação e a gestão de sua infraestrutura.

## Origem do nome

Eu particularmente gosto de usar o termo “Infra Ágil” e a razão é simples, em todos esses anos trabalhando com infraestrutura eu sempre me vi com alguém do time “Infra", por essa razão, sempre que abordei o assunto em palestras e treinamentos esse era o termo que eu usava para descrever o que eu praticava no meu dia-a-dia.

O nome funciona bem para um site e evitará conflito com outros modelos ;)

## Objetivos com o site

Pretendemos usar esse site para organizar experiências de diversos sysadmins em um modelo que consiga ajudar outros profissionais da área.
