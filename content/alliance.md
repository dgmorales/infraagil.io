+++
date = "2016-07-01T11:28:25-03:00"
menu = "main"
title = "Alliance"
type = "About"
weight = -209
+++

## Alliance
##### por: Guto Carvalho

Essa iniciativa foi fortemente incentivada em comunidades e durante conversas com amigos em meetups e conferências de TI, por isso, não posso dizer que sou seu inventor ou criador da Infra Ágil, eu me considero no máximo aquele que organizou as ideias de forma que pudessem ser melhor aproveitadas.

Agora que este site foi finalmente publicado, alguns profissionais que influenciaram sua criação foram convidados para ajudar a manter o conteúdo disponibilizado.

Apresento o coletivo que manterá os ideais e o conteúdo deste modelo.

## Core Members

Guto Carvalho (@gutocarvalho)<br>
Miguel Filho (@mciurcio)<br>
Fernando Ike (@fernandoike)<br>
José Eufrásio Júnior (@coredump)<br>

## Alliance Members

Elias Mussi (@eliasmussi)<br>
Douglas Andrade (@dsandrade)<br>
Rafael Gomes (@gomex)<br>
Christiano Linuxmen (@linuxmen)<br>
