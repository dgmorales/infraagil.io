+++
menu = "main"
title = "Origens"
type = "about"
weight = -230

+++

## As origens do modelo
##### por: guto carvalho

Em 2011 eu dividi uma palestra com o colega Daniel Sobral (a.k.a Gandalf) no CONSEGI em Brasília/DF, o tema central da palestra era a gerência de configurações e os benefícios deste modelo para o Governo Federal. Tivemos um excelente quórum e o tema começou a se espalhar pela Esplanada de Ministérios.

Creio que aquela foi a primeira oportunidade que eu tive para abordar o momento em que eu vivia como sysadmin. O movimento DevOps estava no início da formação de sua cultura, não fazia nem um ano que John Willis e Damon Edwards haviam criado o acrônimo CAMS, iniciativa que deu norte para a comunidade DevOps. Sem nem saber ou conhecer direito este modelo, o time em que eu trabalhava já havia começado a estudar e experimentar algumas técnicas e métodos ágeis na operação, algo inovador dentro do governo brasileiro e principalmente inovador para a organização que havia nos contratado.

#### Marco Zero

Naquela época trabalhei em um time fantástico, pude aprender muito com profissionais experientes como Fernando Ike (@fernandoike), José Eufrásio Júnior (@coredump), Daniel Capó Sobral (@dcsobral), Daniel Negri (@danielnegri), Douglas Andrade (@douglasandrade) dentre outros.

Nós estudávamos constantemente, estávamos sempre experimentando e compartilhando experiências dentro e fora dos times. O @FernandoIke trabalhava na integração de todos os times de TI da organização, focando na comunicação, no compartilhamento de responsabilidades, na garantia de entrega e na disponibilidade de recursos para o time devel trabalhar traquilo e entregar o que o cliente precisava. Ele era o escudo da TI e nos ajudava a manter o foco. O @Coredump era o líder do time de infra, seu foco era inovação e autonomia, graças a ele nosso time começou a estudar automação de infraestrutura no final de 2010, testamos e usamos ali versões primitivas do Puppet e Chef. No final dos testes acabamos optando pelo Puppet e fizemos a primeira implantação em escala de centenas de nós no governo brasileiro.

Em 2010 nós já estávamos trabalhando com infraestrutura como código, gerência de configuração, orquestração, pipeline de deploys e métricas avançadas, respirávamos inovação dia-a-dia, e tudo isso foi construindo em uma curta janela de um ano e meio.   Foi um período fantástico da minha vida em que aprendi muito com todo o time.

Após a conclusão do projeto, cada um seguiu um rumo diferente , alguns foram morar no exterior, outros mudaram de área, outros se tornaram empresários, cada um seguiu um caminho diferente, mas acredito que aquela experiência marcou todos daquele time, no meu caso foi um divisor de águas do profissional que eu era antes e do profissional que eu me tornei após aquela experiência.

#### Novos caminhos, novas parcerias

Alguns anos depois desta experiência, no início de 2014, depois de muitas aventuras pelo Governo Federal, eu conheci  pessoalmente o Miguel Di Ciurcio Filho (@mciurcio), outro desses profissionais que pensam bem a frente do seu tempo. Enquanto eu desbravava datacenters na esplanada dos ministérios em 2010, na mesma época, Miguel atuava com startups e incubadoras na região de Campinas e Barueri no estado de São Paulo. Ele trabalhou com tecnologias modernas e inovadoras na área de automação, banco de dados e virtualização.  Sua experiência com desenvolvimento, em especial no Google Summer of Code, no qual ele atuou com o projeto QEMU, lhe deram um olhar único relativo a administração e gestão de infraestrutura de TI das organizações em que passou.

Todos esses caminhos e interesses comuns, a participação nas mesmas comunidades, os projetos que acompanhamos, tudo isto foi importante para o início de nossa parceria na
Instruct, empresa que ele fundou em 2011 e empresa na qual faço parte desde 2014 após seu convite.

Trabalhando com ele, o espaço e o alcance que tive para estudar e aprimorar este modelo - dentro da Instruct - foi inimaginável, afinal, ambos tivemos experiências muito parecidas, seguimos escolas similares como sysadmins e através da Instruct encontramos as empresas que podiam se beneficiar da soma de nossas experiências.

De lá para cá temos construído e evoluído juntos essa abordagem da infra ágil que agora organizo e compartilho aqui neste site com vocês.
