+++
date = "2016-07-01T07:03:02-03:00"
menu = "main"
title = "Modelo"
type = "about"
weight = -220
+++

##  Infra Ágil

Considerando que DevOps é um conjunto de **boas práticas** reorganizadas dentro de um contexto específico, com um objetivo claro, que faz sentido para uma determinada geração, Infra Ágil também segue a mesma linha e reorganiza boas práticas e soluções em uma ordem que se
mostrou eficaz em muitos cases e projetos de infraestrutura.

### Princípios

Ao longo dos últimos cinco anos identificamos pontos sensíveis de infraestrutura que precisam ser tratados e renovados para que uma infra realmente consiga fazer a transição para algo mais eficiente e ágil.

Os princípios deste modelo envolvem:

1. Automação
2. Entrega
3. Métricas
4. Pessoas

Observe cada princípo do modelo, todos devem ter sua atenção no processo de mudança e renovação do seu time de infraestrutura.
